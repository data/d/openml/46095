# OpenML dataset: Financial_Allocations_Russia-Ukraine_War_2024

https://www.openml.org/d/46095

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Description:
The "Financial Allocations.csv" dataset provides a comprehensive snapshot of financial allocations across different categories including humanitarian, military, and total bilateral allocations, in addition to a country's participation status in the European Union (EU) and their share in EU allocations. This dataset spans five countries: Estonia, Belgium, Czech Republic, Australia, and South Korea. Through its well-structured columns, this dataset sheds light on the financial engagement and priorities of these countries in the realms of humanitarian efforts, military expenditure, and overall financial contributions within bilateral agreements. Additionally, it highlights the share of financial allocations attributed to the EU's budget, thereby offering insights into the economic dynamics and commitments of EU member states versus non-EU countries featured within the dataset.

Attribute Description:
1. Country: Lists the names of countries covered in the dataset.
2. EU member: Indicates whether a country is an EU member (1 for 'yes', 0 for 'no').
3. Financial allocations ($ billion): Represents the financial allocations without specifying the category.
4. Humanitarian allocations ($ billion): Specifies the allocations dedicated to humanitarian efforts.
5. Military allocations ($ billion): Details the allocations destined for military expenditure.
6. Total bilateral allocations ($ billion): Aggregates the total allocations made under bilateral agreements.
7. Share in EU allocations ($ billion): Denotes the country's financial contribution to the EU budget, with 'nan' indicating unavailable data.

Use Case:
This dataset serves as an invaluable resource for policy analysts, economists, and researchers focusing on international relations, economic policymaking, and development studies. It can facilitate an array of analyses, from assessing the financial commitment of EU countries towards humanitarian and military efforts to comparing the economic engagements of EU versus non-EU members. Additionally, this dataset can support studies exploring the impact of financial allocations on diplomatic relations and international cooperation.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/46095) of an [OpenML dataset](https://www.openml.org/d/46095). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/46095/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/46095/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/46095/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

